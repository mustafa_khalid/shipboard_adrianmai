# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 15:45:27 2022

@author: mkm_i
"""

import numpy as np

def find_midpoint(*args):
    
    cropped_img=args[0]
    midP=args[1]
    midP_x=midP[0][0]
    midP_y=midP[0][1]
        
    return cropped_img, midP_x, midP_y



    