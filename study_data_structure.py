# -*- coding: utf-8 -*-
"""
Created on Wed Feb  2 09:05:34 2022

@author: mkm_i
"""

import os
import pandas as pd
from os import listdir
import numpy as np
import cv2 
import matplotlib.pyplot as plt

data_folder=r"C:\Users\mkm_i\Desktop\Cadmatic\ShipboardCase\Data_Shipboard"
xyz_folder_name = os.path.join(data_folder,"Midway_contain_xyz")
pano_folder_name = os.path.join(data_folder,"Midway_contain_bullseye_pics")

name_l = sorted(listdir(xyz_folder_name))
name_p = sorted(listdir(pano_folder_name))

# for name in name_l:
    
name=name_l[0]    
count=0

# Process 3D data
data = pd.read_csv(xyz_folder_name + '/' + name, sep='\s+', header=None)

'''
    data with 8 columns contains grid (u,v) = 0:2; xyz = 2:5; rgb = 5:8
    xyz_grid: is the grid mapping between 2D and 3D
'''

grid_data = data.iloc[:, 0:2].to_numpy()
xyz_data = data.iloc[:, 2:5].to_numpy()
xyz_grid = np.zeros((grid_data[:, 0].max() + 1, grid_data[:, 1].max() + 1, 3))
xyz_grid[grid_data[:, 0], grid_data[:, 1]] = xyz_data

# #Visualize xyz
# pcd = o3d.geometry.PointCloud()
# pcd.points = o3d.utility.Vector3dVector(xyz_data)
rgb=data.iloc[:, 5:8].to_numpy()
# np.vstack((rgb[:,0], rgb[:,1], rgb[:,2])).transpose()
# pcd.colors = o3d.utility.Vector3dVector(rgb/255)
# #pcd.normals = o3d.utility.Vector3dVector(normals)
# o3d.visualization.draw_geometries([pcd])


# Read pano image
image = cv2.imread(pano_folder_name + '/' + name_p[count])


print(f'Rows in uv grid: {grid_data.shape[0]}')
print(f'Number of points in point cloud: {xyz_data.shape[0]}')
print(f'Size of xyz grid: {(xyz_grid.shape[0])*(xyz_grid.shape[1])}')

count_u = np.bincount(grid_data[:,0])
count_v = np.bincount(grid_data[:,1])
# Count occurrence of element '3' in numpygrid_datav
u=grid_data[:,0]
v=grid_data[:,1]
u_max=u.max()
v_max=v.max()
print(f'Expected occurences of umax={u_max} in array: {v_max+1} ')
print(f'Actual occurences of {u_max} in array: {count_u[u_max]} ')
print('----------------------')
print(f'Expected occurences of v_max={v_max} in array: {u_max+1} ')
print(f'Actual occurences of {v_max} in array: {count_v[v_max]} ')


#Find indices where 5175 occurs
umaxids=np.argwhere(u==u_max)
vmaxids=np.argwhere(v==u_max)

plt.figure()
plt.hist(u,bins=np.unique(u))
plt.title('U column in uv grid')

plt.figure()
plt.hist(v,bins=np.unique(v))
plt.title('V column in uv grid')

#%% 3D to 2D unwrapping

frame=np.zeros([xyz_grid.shape[0],xyz_grid.shape[1]])
plt.imshow(frame)
plt.imshow(xyz_grid[:,:,0])
plt.title('2D image from uv grid')
plt.figure()
plt.imshow(image)
plt.title('Panorama image')
